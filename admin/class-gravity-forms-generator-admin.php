<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.onediver.net/
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/admin
 * @author     race34 <raven@sparkfn.com>
 */
class Gravity_Forms_Generator_Admin {

	/**
	 * Spreadsheet Web App Url
	 * 
	 * @since 1.0.0
	 * @access private
	 * @var string $webappurl	The Spreadsheet web app url
	 */
	private static $webappurl = GFG_WEB_APP_URL;
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Generator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Generator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gravity-forms-generator-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Generator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Generator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gravity-forms-generator-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function enqueue_only_on_form () {
		$sheet_list = [];

		try {
			$sheet_list = self::spreadsheetFetch([
				'type' => 'sheet_names_list'
			]);

			if (isset($fields['error'])) {
				$sheet_list = [];
			}

		} catch (Exception $e) {
			// just continue
		}
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gravity-forms-generator-admin.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'gfgscript', [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'sheet_list' => $sheet_list
		]);
	}

	/**
	 * Add Plugin Menu
	 * 
	 * 
	 */
	public function add_plugin_menu () {
		add_menu_page('Create Form', 'Gravity Forms Generate', 'manage_options', $this->plugin_name, array($this, 'display_plugin_settings'));
		add_submenu_page( $this->plugin_name, 'Create Form', 'Create Form', 'manage_options', $this->plugin_name, array($this, 'display_plugin_settings'));
		add_submenu_page( $this->plugin_name, 'Help', 'Help', 'manage_options', $this->plugin_name . '-help', array($this, 'display_plugin_help'));
	}

	public function display_plugin_settings () {
		$this->enqueue_only_on_form();
		include_once 'partials/gravity-forms-generator-admin-display.php';
	}

	public function display_plugin_help () {
		// code here
	}

	/**
	 * Generate Gravity Form
	 */
	public function create_form () {
		if ( isset($_POST['meta_nonce']) && wp_verify_nonce($_POST['meta_nonce'], 'meta_nonce') ) {
			if( isset( $_POST['ajaxrequest'] ) && $_POST['ajaxrequest'] === 'true' ) {
				$fields = isset($_POST['fields']) ? $_POST['fields'] : [];
				$form_name = isset($_POST['form-name']) ? ucfirst(strtolower( $_POST['form-name'] )) : '';
				$sheet_name = isset($_POST['sheet-name']) ? $_POST['sheet-name'] : '';
				$error = [];

				// do not allow traialing spaces or 
				// empty element
				if (trim($form_name) !== '') {
					if (!self::is_form_name_available($form_name))  
						$error[] = 'Form Name is already exists!';
				} else {
					$error[] = 'Form Name should not be empty!';
				}

				try {
					$fields = self::spreadsheetFetch([
						'type' => 'get_sheet_headers',
						'sheet_name' => $sheet_name
					]);

					if (isset($fields['error'])) {
						foreach ($fields['error'] as $err) {
							$error[] = $err['message'];
						}
					}

				} catch (Exception $e) {
					$error[] = $e->getMessage();
				}

				// if has error bail already
				if (!empty($error)) {
					echo json_encode(['error' => $error]);
					wp_die();
				}
				
				/**
				 * After long haul, finally we 
				 * will generate our form
				 * Alrighty.
				 */
				$this->generate_form($form_name, $sheet_name, $fields);

				echo json_encode(['success' => 'Success']);
				wp_die();
            }
		} else {
			echo json_encode(['error' => 'Invalid Nonce!']);
			wp_die();
		}
	}

	public function generate_form ($form_name, $sheet_name, $fields) {
		
		$form = [];
		$cs_field_id = 1;

		$form['title'] = $form_name;
		$form['id'] = $cs_field_id;
		$form['description'] = '';
		$form['labelPlacement'] = 'top_label';
		$form['descriptionPlacement'] = 'below';
		$form['button'] = [
			'type' => 'text',
			'text' => 'Reserve',
			'imageUrl' => '',
			'conditionalLogic' => ''
		];
		$form['gfg'] = true;
		$form['gfg_sheet_name'] = $sheet_name;

		// map inputs
		$inputs =  array_map(function ($input, $key) use ($cs_field_id) {
			$key++;

			return [
				'id' => $cs_field_id . '.' . $key,
				'label' => $input,
				'name' => ''
			];

		}, $fields, array_keys($fields));

		// form fields
		$form['fields'] = [
			[
				'type' => 'chainedselect',
				'id' => $cs_field_id,
				'label' => 'chainedselect name',
				'visibility' => 'visible',
				'inputs' => $inputs,
				'choices' => [],
				'gfcsFile' => [
					'name' => 'empty.csv',
					'type' => 'text/csv',
					'dateUploaded' => 1553074165,
					'isFromFilter' => ''
				]
			]
		];

		$result = GFAPI::add_form( $form );
	}

	/**
	 * GET request on spreadsheet web app
	 * 
	 */
	private static function spreadsheetFetch ($parameters) {
		// convert array to query params
		$parameters = http_build_query($parameters);
		
		$url = self::$webappurl;
		$request = wp_remote_get($url . ($parameters ? '?' . $parameters : ''));
	
		if( is_wp_error( $request ) ) {
			throw new Exception('Google Apps Script Internal Error. Please Try Again!');
		}
		
		// retrieve body
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body, true );

		if (isset($data['data'])) {
			return $data['data']['result'];
		}

		if (isset($data['error'])) {
			return $data;
		}

		throw new Exception('Something Wen\'t wrong!');
		return;
	}

	/**
	 * Check if form name is available
	 */
	private static function is_form_name_available ($form_name) {
		$forms = GFAPI::get_forms();
		$isTrue = true;

		// check if form already exists
		foreach ($forms as $form) {
			$form_title = rgar($form, 'title');
			if ($form_title == $form_name) {
				$isTrue = false;
				break;
			}
		}

		return $isTrue;
	}
}
