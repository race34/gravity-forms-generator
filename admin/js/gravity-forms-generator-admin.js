(function( $ ) {
	'use strict';
	
	var spinner = $('.gfg-spinner');
	var submitBtn = $('#gfg-submit-btn');
	var selectSheetName = $('#gfg-select-sheet-name');

	// Form Submit
	var form = $("#gfg-form");

	// Append sheet Name
	createSheetNameOptions();

	// form listener
	form.on({
		submit: function (e) {
			e.preventDefault();
			spinner.addClass('is-active');
			submitBtn.prop('disabled', true);
			// serialize data
			var form_data = $(this).serialize();

			// add ajaxtrequest is true
			form_data = form_data+'&ajaxrequest=true&submit=Submit+Form';

			$.ajax({
				url: gfgscript.ajax_url,
				type: 'post',
				data: form_data
			})

			.done(function (response) {
				$('.error-notice').html('');
				response = JSON.parse(response);

				// if error occurs
				if (response.hasOwnProperty('error')) {
					response.error.forEach(function (e) {
						$('.error-notice').append(notificationFormat('error', e));
					})
				}

				// if success
				if (response.hasOwnProperty('success')) {
					$('.error-notice').append(notificationFormat('notice-success','Sucessfully Generated!'));
				}

				spinner.removeClass('is-active');
				submitBtn.prop('disabled', false);
			})

			.fail(function (e) {
				$('.error-notice').append(notificationFormat('notice-error', 'Something wen\'t wrong. Internal Error!'));
				spinner.removeClass('is-active');
				submitBtn.prop('disabled', false);
			})
		}
	})

	// gfh click
	$('.wrap').on('click', '.gfg-dismiss', function(e){
		$(this).parent().parent().parent().parent('div').remove(); //Remove tr on table
	});

	/**
	 * Notification Message Format
	 * 
	 * @param {*} type 
	 * @param {*} message 
	 */
	function notificationFormat (type, message) {
		var text = '<div class="notice ' + type + ' is-dismissable">' +
			'<p>' + message + 
				'<span>' + 
					'<small>' + 
						'<a class="gfg-dismiss" href="javascript:void(0);" style="float: right; color: red; text-decoration: none;">dismiss</a>' + 
					'</small>' + 
				'</span>' +
			'</p>' +
		'</div>';

		return text;
	}

	/**
	 * Append options
	 */
	function createSheetNameOptions () {
		if (gfgscript.hasOwnProperty('sheet_list')) {
			var text = '<option></option>';
			var sheet_list = gfgscript.sheet_list;
			
			sheet_list.forEach(function (index) {
				text += '<option value="' + index + '">' + index + '</option> \n';
			});

			selectSheetName.html(text);
		}
	}
})( jQuery );
