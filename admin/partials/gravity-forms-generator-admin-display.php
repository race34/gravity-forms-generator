<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.onediver.net/
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/admin/partials
 */

// create nonce
$wp_nonce = wp_create_nonce('meta_nonce');
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <div class="error-notice">
    </div>
    <?php echo "<h2>" . __( 'Create Form (Chained Selects)' ) . "</h2>";?>
    <form name="gfg-form" id="gfg-form" method="post">
        <input type="hidden" name="action" value="gfg_create_form">
        <input type="hidden" name="meta_nonce" value="<?php echo $wp_nonce; ?>">

        <hr />
        
        <p>
            <h3>Form Name: </h3> 
            <input type="text" name="form-name" style="width: 400px">
        </p>

        <p>
            <h3>Sheet Name: </h3> 
            <select name="sheet-name" id="gfg-select-sheet-name" style="width: 400px"></select>
        </p>
        
        <hr />

        <p class="submit">
            <input style="float: left;" id="gfg-submit-btn" type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Create') ?>" />
            <span style="float: left;" class="gfg-spinner spinner"></span>     
        </p>

    </form>
</div>
