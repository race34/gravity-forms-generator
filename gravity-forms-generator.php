<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.onediver.net/
 * @since             1.0.0
 * @package           Gravity_Forms_Generator
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms Generator
 * Plugin URI:        https://www.onediver.net/
 * Description:       It will generate gravity form and already include chained selects.
 * Version:           1.2
 * Author:            race34
 * Author URI:        https://www.onediver.net/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gravity-forms-generator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GRAVITY_FORMS_GENERATOR_VERSION', '1.2' );
define( 'GFG_WEB_APP_URL', 'https://script.google.com/macros/s/AKfycbwSrlXJF4zOm6v6M7B40WHRL6Xwv23n_G1XZ2wZuKMCzP3u_zQ/exec' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gravity-forms-generator-activator.php
 */
function activate_gravity_forms_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-generator-activator.php';
	Gravity_Forms_Generator_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gravity-forms-generator-deactivator.php
 */
function deactivate_gravity_forms_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-generator-deactivator.php';
	Gravity_Forms_Generator_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gravity_forms_generator' );
register_deactivation_hook( __FILE__, 'deactivate_gravity_forms_generator' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gravity-forms-generator.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gravity_forms_generator() {
	$plugin = new Gravity_Forms_Generator();
	$plugin->run();
}

run_gravity_forms_generator();