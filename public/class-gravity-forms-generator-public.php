<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.onediver.net/
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/public
 * @author     race34 <raven@sparkfn.com>
 */
class Gravity_Forms_Generator_Public {
	
	// $hooks[] = array(
	// 	'hook'          => $hook,
	// 	'component'     => $component,
	// 	'callback'      => $callback,
	// 	'priority'      => $priority,
	// 	'accepted_args' => $accepted_args
	// );

	public $form_id;
	public $form_input = [];
	public $parent_hooks = [];
	public $child_hooks = [];
	/**
	 * Spreadsheet Web App Url
	 * 
	 * @since 1.0.0
	 * @access private
	 * @var string $webappurl	The Spreadsheet web app url
	 */
	private static $webappurl = GFG_WEB_APP_URL;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Generator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Generator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gravity-forms-generator-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravity_Forms_Generator_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravity_Forms_Generator_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
	}

	public function add_shortcode ($atts = "", $component = "") {
		$atts = shortcode_atts( array(
			'form_id' => ''
		), $atts);

		// if (!isset($atts['form_id'])) return; // bail already ;
		wp_enqueue_script( 'gfgscript', plugin_dir_url( __FILE__ ) . 'js/gravity-forms-generator-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script('gfgscript', 'gfg', $atts);

		return;
	}
}
