<?php

class Gravity_Forms_Filter_Loader {
    
    /**
     * Protected Filters
     */
    public $filters;

    public function __construct () {
        $this->filters = array();
    }

    /**
     * ADD filters
     */
    public function add_filter ( $hook, $callback, $priority, $accepted_args ) {
        $this->filters = $this->add($this->filters, $hook, $callback, $priority, $accepted_args);
    }

    /**
     * Contaoner
     */
    private function add( $hooks, $hook, $callback, $priority, $accepted_args ) {

		$hooks[] = array(
			'hook'          => $hook,
			'callback'      => $callback,
			'priority'      => $priority,
			'accepted_args' => $accepted_args
		);

		return $hooks;

    }
    
    /**
     * Run Baby
     */
    public function run () {
        foreach ($this->filters as $filter) {
            add_filter($filter['hook'], $filter['callback'], $filter['priority'], $filter['accepted_args']);
        }
    }

}