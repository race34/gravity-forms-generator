<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-gravity-forms-filter-loader.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/advanced-hooks-api.php';

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.onediver.net/
 * @since      1.0.0
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gravity_Forms_Generator
 * @subpackage Gravity_Forms_Generator/public
 * @author     race34 <raven@sparkfn.com>
 */
class Gravity_Forms_Filter_Generator_Public {

	public $form_id;
	public $form_input = [];
	public $parent_hooks = [];
	public $child_hooks = [];

	/**
	 * Spreadsheet Web App Url
	 * 
	 * @since 1.0.0
	 * @access private
	 * @var string $webappurl	The Spreadsheet web app url
	 */
	private static $webappurl = GFG_WEB_APP_URL;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->loader = new Gravity_Forms_Filter_Loader();
	}

	/**
	 * Hooks will be added upon 
	 * wordpress initialization
	 * 
	 */
	public function init () {
		global $wpdb;
		$id = '';

		if ( !shortcode_exists( 'gravityforms' ) ) {
			return; // bail
		}

		// get form_id from ajax request
		$id = isset($_REQUEST['form_id']) ? $_REQUEST['form_id'] : '';
		if (!$id) {
			// explode url
			$url = explode('?', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
			$postid = url_to_postid($url[0]);

			// explode url by slash
			$url = explode('/', 'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);

			// request uri is always on
			// the 4th element
			$slug = $url[4]; // parent
			
			if (!trim($slug)) return; // bail
			
			// get post id of the current slug
			$post_content = $wpdb->get_var("SELECT post_content FROM $wpdb->posts WHERE post_status='publish' AND post_name = '$slug'");

			$id = (int) $this->get_form_id($post_content); 
		}
		
		// generate hooks
		$this->add_hooks($id);
		
		// add filter trigger
		$this->loader->run();	
		
		return true;	
	}

	/**
	 * Add hooks will generate 
	 * hooks inputs and conditions
	 * 
	 */
	public function add_hooks ($id) {
		// $id = get_query_var('gfg_id');
		// get form object of specific id
		$form = GFAPI::get_form($id);

		// if form is generate by the plugin
		if ( rgar($form, 'gfg')) {
			$form_id = rgar($form, 'id');
			$sheet_name = rgar($form, 'gfg_sheet_name');
			
			// get chained select field object
			$filtered = array_filter(json_decode(json_encode(rgar($form, 'fields')), true), function ($row) {
				return $row['type'] == 'chainedselect' && $row['id'] == 1;
			});

			if (!empty($filtered)) {
				$field_inputs = $filtered[0]['inputs']; // input fields
				$field_id = $filtered[0]['id']; // chained field id
				
				$gfg_child = [];

				// parameter to be pass
				$params = [
					'type' => 'get_data',
					'sheet_name' => $sheet_name
				]; 

				// get fields inputs
				foreach ($field_inputs as $input_key => $input) {
					
					$child_id = str_replace($field_id . '.', '', $input['id']);
					$label = $input['label'];

					// add new input label
					$child_ids[] = ['id' => $child_id, 'label' => $label]; 

					// remove current input label
					$filtered_child_ids = array_filter($child_ids, function ($e) use ($label) {
						return $e['label'] !== $label;
					});

					// set the hook tag
					$filter_name = "gform_chained_selects_input_choices_{$form_id}_{$field_id}_$child_id";

					if ($child_id == 1) {

						// query parameters for first dropdown field
						$parent_params = array_merge($params, [
							'is_pluck' => true,
							'child' => $label
						]);	

						// add filter hook for parent dropdown
						$this->parent_hook($filter_name, $parent_params); 
						continue;
					}

					// child hooks
					$this->child_hook( $filter_name, $input_key, $params, $label, $filtered_child_ids, $child_id);
				}
			}
		}
	}

	/**
	 * Get form id from a post content
	 * 
	 */
	public function get_form_id ($post_content) {
		$pattern = get_shortcode_regex();
		$shortcode = 'gravityform';
		$form = '';
		$form_id = '';

		// if shortcode exists
		if ( preg_match_all( '/'. $pattern .'/s', $post_content, $matches ) && array_key_exists( 2, $matches ) && in_array( $shortcode, $matches[2] ) ) {
			$gravity_shortcode = $matches[0];
			$form = preg_grep ('/^\[gravityform (\w+)/i', $gravity_shortcode); // find gravity form from array
			$form = array_values($form);
		}

		if ($form) {
			$form = str_replace(['[',']'], '', $form[0]); // remove brackets
			$form = explode(' ', $form); // explode str to array
			$form_id = (int) trim(str_replace('id=', '', $form[1])); // element 1 has the id number
		}

		return $form_id;
	}

	/**
	 * Add filter for parent hooks
	 * It will be the first dropdown field
	 * 
	 */
	public function parent_hook ($filter_name, $params) {
		// add filter for parent selects
		$callback = function ($input_choices, $form_id, $field, $input_id, $chain_value, $value, $index ) 
			use ($params, $filter_name) {
				$url = GFG_WEB_APP_URL . '?' . http_build_query($params);

				try {  
					$choices = $this->fetch_choices($url);
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
				}

				return $choices;
			};

		// $this->add_filter_once($filter_name, $callback, 10, 7);
		$this->loader->add_filter($filter_name, $callback, 10, 7);

	}

	/**
	 * Child hooks will be added after the parent hooks
	 * 
	 */
	public function child_hook ($filter_name, $input_key, $params, $child_label, $new_id, $child_id) {

		// add filter for parent selects
		$callback = function ($input_choices, $form_id, $field, $input_id, $chain_value, $value, $index) 
			use ($new_id, $params, $child_label, $filter_name) {
				$url = GFG_WEB_APP_URL . '?' . http_build_query($params);
	
				$selected_make = '';

				// this will get item label of previous 
				// dropdowns
				foreach ($new_id as $item) {

					$id = $item['id'];
					$label = $item['label'];

					// this wil get value of selected dropdown
					$value = $chain_value[ "{$field->id}.$id" ];

					if (!$value) {
						return $input_choices; // bail already;
					}
					
					// add condition label and label value
					$selected_make .= "&condition=$label&$label=$value";
				}

				if( ! $selected_make ) {
					return $input_choices;
				}

				// concat the selected dropdown values
				$url .= "$selected_make&child=$child_label"; 
				
				try {  
					$choices = $this->fetch_choices($url);
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
				}

				return $choices;
			};
		
			// $this->add_filter_once($filter_name, $callback, 10, 7);
			$this->loader->add_filter($filter_name, $callback, 10, 7);
		
	}

	/**
	 * Add filter once
	 * It will remove first one
	 * and return the value
	 *  
	 */
	public function add_filter_once( $hook, $callback, $priority = 10, $args = 1 ) {
		$singular = function () use ( $hook, $callback, $priority, $args, &$singular ) {
			$value = call_user_func_array( $callback, func_get_args() );
			remove_filter( $hook, $singular, $priority, $args );
			return $value;	
		};

		add_filter( $hook, $singular, $priority, $args );
	}

	/**
	 * Fetch Choices on our web app
	 * It will request base on url
	 * So conditions must be put on 
	 * Url as parameters
	 * 
	 */
	public function fetch_choices ($url) {
		$response = wp_remote_get( $url );
		$choices = [];
		
		// if failed
		if ( wp_remote_retrieve_response_code( $response ) != 200 ) {
			return $choices;
		}

		// retrieve data from web app
		try {
			$data = json_decode( wp_remote_retrieve_body( $response ), true )['data']['result'];
		
			foreach( $data as $make ) {
				$choices[] = array(
					'text'       => $make,
					'value'      => $make,
					'isSelected' => false
				);
			}
			
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

		return $choices;
	}
}

